﻿using AForge.Video;
using AForge.Video.DirectShow;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Trabalho2Indes.Properties;

namespace Trabalho2Indes
{
    public partial class Form1 : Form
    {
        private FilterInfoCollection colection;
        private VideoCaptureDevice cam;
        private VideoCaptureDevice liveCam;
        private MJPEGStream stream;

        private Boolean cameraLocalOn;
        private Boolean ipCamOn;
        private Boolean playListOn;
        private Boolean video1On;
        private Boolean video2On;


        String[] files,filesVideo2;
        List<String> paths, video1paths, video2paths;

        private WMPLib.IWMPPlaylist playlist;
        private WMPLib.IWMPMedia media;

        private Boolean playListCreated;

        public Form1()
        {
            InitializeComponent();
            cameraLocalOn = false;
            playListCreated = false;
            ipCamOn = false;
            playListOn = false;
            video1On = false;
            video2On = false;

            this.liveMediaPlayer.Ctlenabled=false;

            playlist = liveMediaPlayer.playlistCollection.newPlaylist("myPlaylist");
            axWindowsMediaPlayer1.uiMode = "none";


        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void cam_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap bit = (Bitmap)eventArgs.Frame.Clone();
            pictureBox1.Image = bit;


        }

        private void cam_NewFrameLive(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap bit = (Bitmap)eventArgs.Frame.Clone();
            livePictureBox.Image = bit;

        }

        private void stream_NewFrameLive(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap bit = (Bitmap)eventArgs.Frame.Clone();
            livePictureBox.Image = bit;

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (colection != null && colection.Count > 0) {
                this.button4.BackColor = Color.LimeGreen;

                cam = new VideoCaptureDevice(colection[comboBox1.SelectedIndex].MonikerString);
                cam.NewFrame += new NewFrameEventHandler(cam_NewFrame);
                cam.Start();

            }
            
        }
        
        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load_1(object sender, EventArgs e)
        {
            colection = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo VideoCaptureDevice in colection)
            {
                comboBox1.Items.Add(VideoCaptureDevice.Name);
            }
            paths = new List<string>();
            axWindowsMediaPlayer2.uiMode = "none";
            axWindowsMediaPlayer3.uiMode = "none";
            video1paths = new List<string>();
            video2paths = new List<string>();
            ImportVideoList(listVideo1, @".\video1", "*.*", video1paths);
           // ImportVideoList(listVideo2, @".\video2", "*.avi", video2paths);
        }

        private void ImportVideoList(ListBox lsb, string Folder, string FileType, List<string> listar)
        {
            DirectoryInfo dinfo = new DirectoryInfo(Folder);
            FileInfo[] Files = dinfo.GetFiles(FileType);
            foreach (FileInfo file in Files)
            {
                lsb.Items.Add(System.IO.Path.GetFileNameWithoutExtension(file.Name));
                listar.Add(file.FullName);
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (cam != null) { 
                if (!cameraLocalOn)
                {
                    livePictureBox.BackgroundImage = null;
                    liveMediaPlayer.Ctlcontrols.stop();
                    liveMediaPlayer.Hide();

                    this.button1.BackColor = Color.LimeGreen;
                    this.btnoffline.BackColor = Color.Crimson;
                    this.button9.BackColor = Color.Crimson;
                    this.button2.BackColor = Color.Crimson;
                    this.button12.BackColor = Color.Crimson;
                    this.button3.BackColor = Color.Crimson;
                    if (stream != null)
                    {
                        stream.Stop();
                        stream.NewFrame += new NewFrameEventHandler(clearLiveFrame);
                    }

                    if (!cam.IsRunning)
                    {
                        cam.Start();
                    }
                    cam.NewFrame += new NewFrameEventHandler(cam_NewFrameLive);
                    cameraLocalOn = true;

                }
                else {
                    this.button1.BackColor = Color.Crimson;
                    cam.NewFrame += new NewFrameEventHandler(clearLiveFrame);
                    cameraLocalOn = false;
                }

            }
        }

        //Botão VIDEO1
        private void button2_Click(object sender, EventArgs e)
        {
            if (listVideo1.SelectedIndex >= 0)
            {
                this.liveMediaPlayer.uiMode = "none";
                if (!video1On)
                {
                    video1On = true;
                    liveMediaPlayer.Show();
                    this.button2.BackColor = Color.LimeGreen;
                    this.liveMediaPlayer.Visible = true;

                    liveMediaPlayer.URL = video1paths[listVideo1.SelectedIndex];
                    liveMediaPlayer.settings.autoStart = false;
                    liveMediaPlayer.Ctlcontrols.play();

                    axWindowsMediaPlayer2.Ctlcontrols.stop();
                    axWindowsMediaPlayer2.Ctlcontrols.play();

                    this.btnoffline.BackColor = Color.Crimson;
                    this.button9.BackColor = Color.Crimson;
                    this.button1.BackColor = Color.Crimson;

                    this.button12.BackColor = Color.Crimson;
                    this.button3.BackColor = Color.Crimson;
                }
                else {
                    video1On = false;
                    button2.BackColor = Color.Crimson;
                    liveMediaPlayer.Ctlcontrols.stop();
                }
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(textBox1.Text)) {
                //stream = new MJPEGStream("http://88.53.197.250/axis-cgi/mjpg/video.cgi?resolution=320x240/");
                //MJPEGStream stream = new MJPEGStream("https://www.youtube.com/embed/8Hn_V9chnGs/");
                stream = new MJPEGStream(textBox1.Text);
                stream.NewFrame += new NewFrameEventHandler(videoSource_NewFrame);
                stream.Start();
                button7.BackColor = Color.LimeGreen;
            }
        }

        void videoSource_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap image = (Bitmap)eventArgs.Frame.Clone();
            pictureBox5.Image = image;
        }

        void clearIpFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap image = null;
            pictureBox5.Image = image;
        }

        void clearWebCamFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap image = null;
            pictureBox1.Image = image;
        }

        void clearLiveFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap image = null;
            livePictureBox.Image = image;
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Multiselect = true;
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                files = openFileDialog1.SafeFileNames;
                String[] auxPath = openFileDialog1.FileNames;

                for (int k = 0; k < auxPath.Length; k++)
                {
                    paths.Add(auxPath[k]);

                    if (playListCreated) { 
                        media = axWindowsMediaPlayer1.newMedia(auxPath[k]);
                        axWindowsMediaPlayer1.currentPlaylist.appendItem(media);
                    }
                }

                for (int i = 0; i < files.Length; i++)
                {
                    listboxvideos.Items.Add(files[i]);

                }

                if (!playListCreated) { 
                    listboxvideos.SelectedIndex = 0;
                }


                if (!playListCreated)
                {
                    for (int i = 0; i < paths.Count; i++)
                    {
                        media = axWindowsMediaPlayer1.newMedia(paths[i]);
                        playlist.appendItem(media);
                        axWindowsMediaPlayer1.currentPlaylist = playlist;
                        axWindowsMediaPlayer1.settings.autoStart = false;
                        playListCreated = true;
                    }
                }

            }
            
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (listboxvideos.SelectedIndex >= 0) {
                this.button6.BackColor = Color.LimeGreen;
                for (int i = 0; i < paths.Count; i++)
                {
                    media = axWindowsMediaPlayer1.newMedia(paths[i]);
                    playlist.appendItem(media);
                    axWindowsMediaPlayer1.currentPlaylist = playlist;
                    axWindowsMediaPlayer1.Ctlcontrols.play();
                    listboxvideos.SetSelected(i, true);

                }
            }


        }

        private Object getPlaylistItem(String listboxName) {

            Object item=null;
            String playlistItemName;
            string[] ret = listboxName.Split('.');
            for (int i = 0; i < axWindowsMediaPlayer1.currentPlaylist.count; i++) {
                playlistItemName = axWindowsMediaPlayer1.currentPlaylist.Item[i].name;


                if (String.Equals(playlistItemName, ret[0], StringComparison.Ordinal)) {
                    item = axWindowsMediaPlayer1.currentPlaylist.Item[i];
                }
            }
            return item;
        }

        private void btnremove_Click(object sender, EventArgs e)
        {

            WMPLib.IWMPMedia item =(WMPLib.IWMPMedia) getPlaylistItem(listboxvideos.GetItemText(listboxvideos.SelectedItem));
            if (item != null) {
                axWindowsMediaPlayer1.currentPlaylist.removeItem(item);
            }
            listboxvideos.Items.Remove(listboxvideos.SelectedItem);
            

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void axWindowsMediaPlayer1_Enter(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
           
            
        }

        //Botão PLAYLIST
        private void button3_Click(object sender, EventArgs e)
        {
            if (listboxvideos.SelectedIndex >= 0)
            {
                if (!playListOn)
                {
                    this.button3.BackColor = Color.LimeGreen;
                    this.liveMediaPlayer.Visible = true;
                    this.liveMediaPlayer.uiMode = "none";
                    this.button6.BackColor = Color.LimeGreen;
                    this.btnoffline.BackColor = Color.Crimson;
                    this.button9.BackColor = Color.Crimson;
                    this.button1.BackColor = Color.Crimson;
                    this.button2.BackColor = Color.Crimson;
                    this.button12.BackColor = Color.Crimson;

                    liveMediaPlayer.currentPlaylist = axWindowsMediaPlayer1.currentPlaylist;
                    liveMediaPlayer.settings.autoStart = false;

                    axWindowsMediaPlayer1.Ctlcontrols.stop();
                    axWindowsMediaPlayer1.Ctlcontrols.play();

                    liveMediaPlayer.Ctlcontrols.play();


                    liveMediaPlayer.settings.mute = false;
                    playListOn = true;
                }
                else {
                    playListOn = false;
                    button3.BackColor = Color.Crimson;
                    liveMediaPlayer.Ctlcontrols.stop();


                }
            }
        }

        private void stopPlaylist() {

            this.button3.BackColor = Color.Crimson;
            this.liveMediaPlayer.URL = null;
            this.liveMediaPlayer.Visible = false;

        }

        private void btnoffline_Click(object sender, EventArgs e)
        {

            axWindowsMediaPlayer1.Ctlcontrols.stop();
            axWindowsMediaPlayer2.Ctlcontrols.stop();
            axWindowsMediaPlayer3.Ctlcontrols.stop();
            liveMediaPlayer.Ctlcontrols.stop();

            if (cam != null) {
                cam.Stop();
                cam.NewFrame += new NewFrameEventHandler(clearWebCamFrame);
            }

            if (stream != null) {
                stream.Stop();
                stream.NewFrame += new NewFrameEventHandler(clearIpFrame);
            }

            livePictureBox.Image = Resources.Offline;


            this.btnoffline.BackColor = Color.Crimson;
            this.button9.BackColor = Color.Crimson;
            this.button1.BackColor = Color.Crimson;
            this.button2.BackColor = Color.Crimson;
            this.button12.BackColor = Color.Crimson;
            this.button3.BackColor = Color.Crimson;

            this.button4.BackColor = Color.Crimson;
            this.button5.BackColor = Color.Crimson;
            this.button6.BackColor = Color.Crimson;
            this.button7.BackColor = Color.Crimson;
            this.button13.BackColor = Color.Crimson;
            liveMediaPlayer.Visible = false;
           

        }

        /**
        Botão de ativação da IP CAM
        */
        private void button9_Click(object sender, EventArgs e)
        {
            if (stream != null) {
                if (!ipCamOn)
                {
                    livePictureBox.BackgroundImage = null;
                    liveMediaPlayer.Ctlcontrols.stop();
                    liveMediaPlayer.Hide();

                    this.button9.BackColor = Color.LimeGreen;
                    this.button7.BackColor = Color.LimeGreen;
                    this.btnoffline.BackColor = Color.Crimson;
                    this.button1.BackColor = Color.Crimson;
                    this.button2.BackColor = Color.Crimson;
                    this.button12.BackColor = Color.Crimson;
                    this.button3.BackColor = Color.Crimson;

                    if (!stream.IsRunning) {
                        stream.Start();
                    }

                    if (cam != null) {
                        cam.Stop();
                        cam.NewFrame += new NewFrameEventHandler(clearLiveFrame);

                    }
                    stream.NewFrame += new NewFrameEventHandler(stream_NewFrameLive);
                    ipCamOn = true;
                }
                else {
                    
                    stream.NewFrame += new NewFrameEventHandler(clearLiveFrame);
                    this.button9.BackColor = Color.Crimson;
                    ipCamOn = false;
                }
            }
        }

       
        private void liveMediaPlayer_Enter(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (listVideo2.SelectedIndex >= 0)
            {
                if (!video2On)
                {
                    video2On = true;
                    liveMediaPlayer.Show();
                    this.button12.BackColor = Color.LimeGreen;
                    this.liveMediaPlayer.Visible = true;
                    this.liveMediaPlayer.uiMode = "none";

                    //Atribui o url ao player
                    //Configura o autostart para falso
                    //fazemos play. Deta forma conseguimos controlar o play
                    liveMediaPlayer.URL = video2paths[listVideo2.SelectedIndex];
                    liveMediaPlayer.settings.autoStart = false;
                    liveMediaPlayer.Ctlcontrols.play();

                    //Para sincronizar as duas janelas com a mesma imagem
                    axWindowsMediaPlayer3.Ctlcontrols.stop();
                    axWindowsMediaPlayer3.Ctlcontrols.play();

                    this.btnoffline.BackColor = Color.Crimson;
                    this.button9.BackColor = Color.Crimson;
                    this.button1.BackColor = Color.Crimson;
                    this.button2.BackColor = Color.Crimson;
               
                    this.button3.BackColor = Color.Crimson;
                }
                else
                {
                    video2On = false;
                    button12.BackColor = Color.Crimson;
                    liveMediaPlayer.Ctlcontrols.stop();
                }
            }
        }

        private void listVideo1_SelectedIndexChanged(object sender, EventArgs e)
        {
            axWindowsMediaPlayer2.URL = video1paths[listVideo1.SelectedIndex];
            axWindowsMediaPlayer2.settings.mute = true;

            this.button5.BackColor = Color.LimeGreen;

        }

        private void listVideo2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listVideo2.SelectedIndex >= 0)
            { 
                axWindowsMediaPlayer3.URL = video2paths[listVideo2.SelectedIndex];
                axWindowsMediaPlayer3.settings.mute=true;
                this.button13.BackColor = Color.LimeGreen;
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Multiselect = true;
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                filesVideo2 = openFileDialog1.SafeFileNames;
                String[] auxPath = openFileDialog1.FileNames;

                for (int k = 0; k < auxPath.Length; k++)
                {
                    video2paths.Add(auxPath[k]);
                }

                for (int i = 0; i < filesVideo2.Length; i++)
                {
                    listVideo2.Items.Add(filesVideo2[i]);

                }
            }
        }

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            if (listboxvideos.SelectedIndex >= 0)
            {
                this.button6.BackColor = Color.LimeGreen;
                axWindowsMediaPlayer1.settings.mute = true;
                axWindowsMediaPlayer1.Ctlcontrols.play();

                axWindowsMediaPlayer1.CurrentItemChange += new AxWMPLib._WMPOCXEvents_CurrentItemChangeEventHandler(axWindowsMediaPlayer1_CurrentItemChange);
                this.pictureBox2.Visible = false;
            }

        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.Ctlcontrols.stop();
            this.pictureBox2.Visible = true;
        }

        private void listboxvideos_SelectedIndexChanged(object sender, EventArgs e)
        {
            //String newitemName;
            //if (!playListCreated)
            //{
            //    newitemName = listboxvideos.GetItemText(listboxvideos.Items[0]);
            //}
            //else {
            //    newitemName = listboxvideos.GetItemText(listboxvideos.Items[listboxvideos.SelectedIndex]);
            //}

            //string[] ret = newitemName.Split('.');

            //for (int i = 0; i < axWindowsMediaPlayer1.currentPlaylist.count; i++) {

            //    String name = axWindowsMediaPlayer1.currentPlaylist.Item[i].name;
            //    if (String.Equals(ret[0], name, StringComparison.Ordinal)) {
            //        axWindowsMediaPlayer1.currentMedia = axWindowsMediaPlayer1.currentPlaylist.Item[i];
            //    }
            //}
        }

        private void button16_Click(object sender, EventArgs e)
        {
            this.BackgroundImage = Resources.fundo3;

        }

        private void button18_Click(object sender, EventArgs e)
        {
            this.BackgroundImage = Resources.fundo4;
        }

        private void button17_Click(object sender, EventArgs e)
        {
            this.BackgroundImage = Resources.fundo5;
        }

        private void label2_Click_1(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click_1(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.Ctlcontrols.next();
            liveMediaPlayer.Ctlcontrols.next();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.Ctlcontrols.previous();
            liveMediaPlayer.Ctlcontrols.previous();
        }

        private void btnV2Remove_Click(object sender, EventArgs e)
        {
            if (listVideo2.SelectedIndex >= 0) {
                video2paths.RemoveAt(listVideo2.SelectedIndex);
                listVideo2.Items.Remove(listVideo2.Items[listVideo2.SelectedIndex]);
                axWindowsMediaPlayer3.Ctlcontrols.stop();

            }
        }

        private void axWindowsMediaPlayer3_Enter(object sender, EventArgs e)
        {

        }

        private void button15_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";

            if (open.ShowDialog() == DialogResult.OK)
            {

                Bitmap newImage = new Bitmap(open.FileName);
                button15.BackgroundImage = newImage;
                button15.BackgroundImageLayout = ImageLayout.Stretch;
                Bitmap thumb = (Bitmap)newImage.GetThumbnailImage(64, 64, null, IntPtr.Zero);
                this.Icon = Icon.FromHandle(thumb.GetHicon());
            }
        }

        private void axWindowsMediaPlayer1_CurrentItemChange(object sender, AxWMPLib._WMPOCXEvents_CurrentItemChangeEvent e)
        {
            String name = axWindowsMediaPlayer1.currentMedia.name;
            String itemName = "";


            int indexSelected = 0;

            for (int i = 0; i < listboxvideos.Items.Count; i++)
            {
                itemName = listboxvideos.GetItemText(listboxvideos.Items[i]);

                string[] ret = itemName.Split('.');

                if (String.Equals(ret[0], name, StringComparison.Ordinal))
                {
                    indexSelected = i;
                    break;
                }
            }

            listboxvideos.SetSelected(indexSelected, true);
        }

    private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }
    }
}
